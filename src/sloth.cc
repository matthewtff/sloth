#include <iostream>

#include "koohar.hh"

int main() {
  koohar::ServerAsio asio_server(8000);
  asio_server.Listen([&asio_server](
      koohar::Request&& req,
      koohar::Response&& res) {
    res.WriteHead(200);
    res.End("Hello world!");
    if (req.Uri() == "/exit") {
      asio_server.Stop();
    }
  });
}
