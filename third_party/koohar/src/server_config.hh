#ifndef koohar_server_config_hh
#define koohar_server_config_hh

#include <string>
#include <unordered_map>
#include <vector>

namespace koohar {

class Request;

class ServerConfig {
 public:
  void SetStaticDir(const std::string& directory) {
    static_dir_ = directory;
  }
  void SetStaticUrl(const std::string& url) {
    static_urls_.push_back(url);
  }
  void SetUseSSL(const bool use_ssl) {
    use_ssl_ = use_ssl;
  }

  bool IsStaticUrl(const Request& req) const;
  std::string GetStaticDir() const { return static_dir_; }
  bool GetUseSSL() const { return use_ssl_; }

  void Load(const std::string& file_name);

  // Returns empty string if no page for specified code found.
  std::string GetErrorPage(const unsigned short code) const;

private:
  using StringList = std::vector<std::string> ;
  using ErrorPagesMap = std::unordered_map<unsigned short, std::string>;

  std::string static_dir_;
  StringList static_urls_;
  ErrorPagesMap error_pages_;
  bool use_ssl_ = false;
};  // class ServerConfig

}  // namespace koohar

#endif  // koohar_server_config_hh
