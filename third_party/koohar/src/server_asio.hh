#ifndef koohar_server_asio_hh
#define koohar_server_asio_hh

#include <asio.hpp>

#include "http_connection.hh"
#include "server_config.hh"

namespace koohar {

class ServerAsio : public ServerConfig {
public:
  ServerAsio() : ServerAsio(80) {}
  explicit ServerAsio(const unsigned short port);

  virtual ~ServerAsio() = default;

  void Listen(HttpConnection::UserFunc user_call_function);

  unsigned short Port() const { return port_; }

  void Stop() { io_service_.stop(); }

private:
  bool Accept();
  void HandleAccept(HttpConnection::Pointer connection,
                    const std::error_code& error);

private:
  const unsigned short port_;
  asio::io_service io_service_;
  asio::ip::tcp::acceptor acceptor_;

  HttpConnection::UserFunc user_call_function_;
};  // class ServerAsio

}  // namespace koohar

#endif // koohar_server_asio_hh
