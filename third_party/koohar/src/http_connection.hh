#ifndef koohar_http_connection_hh
#define koohar_http_connection_hh

#include <functional>
#include <list>
#include <map>
#include <memory>
#include <string>

#include <asio.hpp>

#include "server_config.hh"
#include "request.hh"

namespace koohar {

class Response;

class HttpConnection : public std::enable_shared_from_this<HttpConnection> {
 public:
  using Pointer = std::shared_ptr<HttpConnection>;
  using UserFunc =
      std::function<void(koohar::Request&&, koohar::Response&&)>;

  static const unsigned int kMaxRequestSize = 65536; // 64KB

  static Pointer Create(asio::io_service& io_service,
                        UserFunc user_call_function,
                        const ServerConfig& config);

  asio::ip::tcp::socket& Socket() { return socket_; }
  void Start();
  void Write(const char* Data, const std::size_t Size);
  void Close() { close_socket_ = true; }
  void SetUserFunction(UserFunc user_call_function);

private:
  using DataBuffers = std::list<std::vector<char>>;

  HttpConnection(asio::io_service& io_service,
                 UserFunc user_call_function,
                 const ServerConfig& config);

  void SendNextBuffer();

  void HandleRead(const std::error_code& error,
                  const std::size_t bytes_transeffed);
  void HandleWrite(const std::error_code& error,
                   const std::size_t bytes_transeffed);

  ServerConfig config_;
  asio::ip::tcp::socket socket_;
  char request_buffer_[kMaxRequestSize];

  Request request_;
  UserFunc user_call_function_;
  DataBuffers buffers_;
  size_t writing_operations_ = 0;
  bool close_socket_ = false;
}; // class HttpConnection

} // namespace koohar

#endif // koohar_http_connection_hh
