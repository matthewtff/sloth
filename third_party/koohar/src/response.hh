#ifndef koohar_response_hh
#define koohar_response_hh

#include <string>
#include <unordered_map>

#include "base/file.hh"
#include "base/json.hh"
#include "base/utils.hh"
#include "http_connection.hh"

namespace koohar {

class Sender;

class Response {
 public:
  using StateMap = std::unordered_map<unsigned short, std::string>;

  explicit Response(HttpConnection::Pointer connection);
  Response(Response&&) = default;

  Response& operator=(Response&&) = default;

  /**
   * Sets and sends appropriate http status to client.
   */
  void WriteHead(const unsigned short State);

  /**
   * Sets header. Can be called multiple times.  Iff Replace set to false,
   * the value whould be added using semicolon.
   */
  void Header(const std::string& header_name,
              const std::string& header_value,
              const bool replace = true);

  /**
   * Calls Header() method using 'Set-Cookie' HeaderName.
   * Actually you should be very carefull to this method cause it
   * does not seem to replace existing cookie value.
   */
  bool Cookie(const std::string& cookie_name,
              const std::string& cookie_value);

  /**
   * Send some data. Can be called multiple times.
   */
  void Body(const std::string& data);
  void Body(const void* buffer, const off_t size);

  /**
   * Cannot be used whith Body() method. After using you still should call
   * the End() method.
   */
  bool SendFile(const File& file);
  bool SendFile(const File& file,
                const off_t size,
                const off_t offset);
  /**
   * Terminates connection to client.
   */
  void End(const std::string& data);
  void End(const void* buffer, const off_t size);
  void End();

  /**
   * Sends redirecting header and 302 (Redirect) status. You still can send
   * some data using Body() method and even set additional headers. Do not
   * forget to close connection using End().
   */
  void Redirect(const std::string& url);

  void SendJSON(const JSON::Object& object);

  void BadRequest();

  /**
   * States are very often used, so no need to create(allocate) that much
   * memory every time response object created. Thats why static map
   * is created.
   */
  static StateMap states;

private:
  /**
   * Body() and End() methods have some identical code, so it moved to
   * private method.
   */
  void Transfer(const void* buffer, const off_t size);
  void Transfer(const std::string& str);

  template <size_t N>
  void TransferString(const char (&str) [N]) {
    Transfer(reinterpret_cast<const void*>(str),
             static_cast<off_t>(string_length(str)));
  }

  /**
   * This method handle '\n\r' sending after headers part and setting
   * appropriate value to headers_allowed variable.
   */
  void SendHeaders();

private:
  StringMap headers_;
  bool headers_allowed_ = true;  // true, till any part of body is sent.
  
  HttpConnection::Pointer connection_;
};  // class Response

};  // namespace koohar

#endif  // koohar_response_hh
