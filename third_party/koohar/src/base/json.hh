#ifndef koohar_json_hh
#define koohar_json_hh

#include <map>
#include <vector>
#include <string>
#include <cstddef>
#include <sstream>
#include <type_traits>

namespace koohar {

namespace JSON {

enum class Type {
  Undefined,
  Boolean,
  Integer,
  Float,
  String,
  Array,
  Collection
};

class Object {
 public:
  Object ()
      : state_(OnValue),
        type_(Type::Undefined) {
  }

  explicit Object(const bool value)
      : type_(Type::Boolean),
        boolean_(value) {
  }

  explicit Object(const long integral)
      : type_(Type::Integer),
        integer_(integral) {
  }

  explicit Object(const double floating)
      : type_(Type::Float),
        float_(floating) {
  }

  explicit Object(const std::string& str)
      : type_(Type::String),
        string_(str) {
  }

  Type GetType() const { return type_; }
  bool HasType(const Type type) const { return type == type_; }

  bool& GetBoolean() { return boolean_; }
  bool GetBoolean() const { return boolean_; }
  bool SetBoolean(const bool value);

  long& GetIntegral() { return integer_; }
  long GetIntegral() const { return integer_; }
  bool SetIntegral(const long value);
  template <typename Trivial>
  bool SetTrivial(Trivial value, std::true_type, std::false_type);

  double& GetFloat() { return float_; }
  double GetFloat() const { return float_; }
  bool SetFloat (const double floating);
  template <typename Trivial>
  bool SetTrivial(Trivial value, std::false_type, std::true_type) {
    return SetFloat(static_cast<double>(value));
  }

  std::string& GetString() { return string_; }
  const std::string& GetString() const { return string_; }
  bool SetString(const std::string& str);
  template <typename Trivial>
  bool SetTrivial(const Trivial& value, std::false_type, std::false_type) {
    return SetString(value);
  }

  void Clear();
  template <typename Trivial>
  Object& operator=(const Trivial& Value);

  std::vector<Object>& GetArray() { return array_; }
  const std::vector<Object>& GetArray() const { return array_; }
  bool SetArray (const std::vector<Object>& objects);
  bool AddToArray (const Object& object);
  bool Remove (const std::size_t index);
  Object& operator[](const std::size_t index);

  std::map<std::string, Object>& GetCollection() { return collection_; }
  const std::map<std::string, Object>& GetCollection() const {
    return collection_;
  }
  bool SetCollection (const std::map<std::string, Object>& ObjCollection);
  bool AddToCollection (const std::string& name, const Object& Obj);
  bool Remove (const std::string& name);
  Object& operator[](const std::string& name);

  bool Empty () const {
    return state_ == OnValue && type_ == Type::Undefined;
  }
  std::string ToString() const;
  std::size_t Parse(const std::string& stream);
  bool ErrorParsing() const { return state_ != OnSuccess; }

private:
  template <typename Type>
  class is_boolean {
  public:
    static const bool value = false;
  };  // class is_boolean<Type>

  enum State {
    OnValue,
    OnArrayObject,
    OnComma,
    OnName,
    OnColon,
    OnCollectionObject,
    OnSuccess,
    OnError
  }; // enum State

private:
  void SetIfUndefined (const Type new_type) {
    if (type_ == Type::Undefined)
      type_ = new_type;
  }
  template <typename Trivial>
  std::string TrivialToString(const Trivial& value) const {
    std::stringstream stream;
    stream << value;
    return stream.str();
  }
  std::string StringToString() const;
  std::string ArrayToString() const;
  std::string CollectionToString() const;
  static bool IsSeporator(const char ch);
  static bool IsBoolean(const std::string& token);
  static bool IsIntegral(const std::string& token);
  static bool IsFloat(const std::string& token);
  static bool IsString(const std::string& token);

  void ParseValue(const std::string& stream, std::size_t& parsed);
  void ParseArrayObject(const std::string& stream, std::size_t& parsed);
  void ParseComma(const std::string& stream, std::size_t& parsed);
  void ParseName(const std::string& stream, std::size_t& parsed);
  void ParseColon(const std::string& stream, std::size_t& parsed);
  void ParseCollectionObject(const std::string& stream,
                             std::size_t& parsed);

private:
  State state_ = OnSuccess;
  std::string token_;
  Type type_;
  bool boolean_ = false;
  long integer_ = 0L;
  double float_ = 0.;
  std::string string_;
  std::vector<Object> array_;
  std::map<std::string, Object> collection_;
}; // class Object

template <>
class Object::is_boolean<bool> {
public:
  static const bool value = true;
}; // class is_boolean<bool>

template <typename Trivial>
bool Object::SetTrivial(Trivial value, std::true_type, std::false_type) {
  return is_boolean<Trivial>::value ?
      SetBoolean(value) :
      SetIntegral(static_cast<long>(value));
}

template <typename Trivial>
Object& Object::operator= (const Trivial& value)
{
   SetTrivial(value, typename std::is_integral<Trivial>::type(),
     typename std::is_floating_point<Trivial>::type());
   return *this;
}

std::string Strigify(const Object& object);
Object Parse(const std::string& stream);

}  // namespace JSON

}  // namespace koohar

#endif  // koohar_json_hh
