#ifndef koohar_handle_hh
#define koohar_handle_hh

#ifdef _WIN32

#include <winsock2.h>

#else /* _WIN32 */

#include <fcntl.h>

#endif /* _WIN32 */

#include <ctime>
#include <string>

namespace koohar {

class File {
 public:

#ifdef _WIN32

  enum class AccessType : DWORD {
    ReadOnly = GENERIC_READ;
    WriteOnly = GENERIC_WRITE;
    ReadWrite = (GENERIC_READ | GENERIC_WRITE);
  };

  using Handle = HANDLE;

#else /* _WIN32 */

  enum class AccessType : int {
    ReadOnly = O_RDONLY,
    WriteOnly = O_WRONLY,
    ReadWrite = O_RDWR
  };

  using Handle = int;

#endif /* _WIN32 */

  enum Error {
    IOError = -1,
  };

  // TODO(matthewtff): Where the are async versions?!
  static int Read(Handle handle, void* buffer, const size_t length);
  static int Write(Handle handle, const void* buffer, const size_t length);
  static bool IsDirectory(const std::string& path) {
    return IsDirectory(path.c_str());
  }
  static bool IsDirectory(const char* path);

  File() = default;
  explicit File(File::Handle handle);
  // This constructor just assigns filename, but file is NOT BEING OPENED.
  explicit File(const std::string& FileName);
  ~File();

  bool Open(AccessType mode);
  void Close();
  void Remove();
  bool Move(const std::string& new_handle_name);
  int Read(void* buffer, const size_t length) const;
  int Write(const void* buffer, const size_t length);
  std::string ReadToString() const;
  size_t Size() const { return size_; }
  std::time_t Time() const { return time_; }
  Handle GetHandle() const { return handle_; }

private:
  void GetInfo();
  void CreateTemp();

  Handle handle_;
  std::string name_;
  size_t size_ = 0;
  time_t time_ = 0;
  bool opened_ = false;
};  // class File

}  // namespace koohar

#endif  // koohar_handle_hh
