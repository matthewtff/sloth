#include "base/file.hh"

#ifndef _WIN32

#include <cstdio>
#include <cstdlib>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>


#endif /* !_WIN32 */

#include <cerrno>
#include <cstring>
#include <vector>

#include "base/utils.hh"

namespace koohar {

File::File(Handle handle) : handle_(handle) {
}

File::File(const std::string& file_name) : name_(file_name) {
}

File::~File() {
  if(opened_)
    Close();
}

bool File::Open(AccessType mode) {
  if (opened_)
    return true;
#ifdef _WIN32

  handle_ = CreateFileA(name_.c_str(), Mode, FILE_SHARE_READ, NULL,
                        OPEN_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL);
  if (handle_ == INVALID_HANDLE_VALUE) {

#else /* _WIN32 */

  if ((handle_ = open(name_.c_str(), static_cast<int>(mode))) == -1) {

#endif /* _WIN32 */

    LOG << "Error opening file " << name_ << " : " <<
        strerror(errno) << std::endl;
    return false;
  }
  opened_ = true;
  GetInfo();
  return true;
}

void File::Close() {
  if (!opened_)
    return;
#ifdef _WIN32

  CloseHandle(handle_);

#else /* _WIN32 */

  close (handle_);

#endif /* _WIN32 */
  size_ = 0;
  time_ = 0;
  opened_ = false;
}

void File::Remove() {
  if (opened_)
    return;
#ifdef _WIN32

  if (!DeleteFileA(name_.c_str())) {

#else /* _WIN32 */

  if (unlink(name_.c_str()) == -1) {

#endif /* _WIN32 */

    LOG << "Unable to remove file " << name_ << std::endl;
  }
}

bool File::Move(const std::string& new_file_name) {
  if (opened_)
    return false;
#ifdef _WIN32

  if (!MoveFileA(name_.c_str(), new_file_name.c_str())) {

#else /* _WIN32 */

  if (rename(name_.c_str(), new_file_name.c_str()) == -1) {

#endif /* _WIN32 */

    LOG << "Unable move file " << name_ << std::endl;
    return false;
  }
  name_ = new_file_name;
  return true;
}

int File::Read(void* buffer, const size_t length) const {
  return opened_ ? File::Read(handle_, buffer, length) : IOError;
}

int File::Write(const void* buffer, const size_t length) {
  return opened_ ? File::Write(handle_, buffer, length) : IOError;
}

std::string File::ReadToString() const {
  if (!opened_)
    return std::string();

  std::vector<char> data(size_);
  const int readed = Read(static_cast<void*>(data.data()), size_);
  if (readed < 0 || readed > static_cast<int>(size_))
    return std::string();

  return std::string(data.begin(), data.end());
}

// static
int File::Read(File::Handle handle, void* buffer, const size_t length) {
#ifdef _WIN32

  DWORD return_value = 0;
  if (!ReadFile(handle,
                buffer,
                static_cast<DWORD>(length),
                &return_value,
                NULL)) {
    return IOError;
  }
#else /* _WIN32 */

  const int return_value = read(handle, buffer, length);

#endif /* _WIN32 */

  return static_cast<int>(return_value);
}


int File::Write(File::Handle handle,
                const void* buffer,
                const size_t length) {
#ifdef _WIN32

  DWORD return_value = 0;
  if (!WriteFile(handle,
                 buffer,
                 static_cast<DWORD>(length),
                 &return_value,
                 NULL)) {

#else /* _WIN32 */

  const int return_value = ::write(handle, buffer, length);
  if (return_value == -1) {

#endif /* _WIN32 */

    LOG << "Error writing to file." << std::endl;
    return IOError;
  }
  return static_cast<int>(return_value);
}

bool File::IsDirectory (const char* path) {
#ifdef _WIN32
  DWORD file_attributes = GetFileAttributes(path);
  if (file_attributes != INVALID_FILE_ATTRIBUTES)
    return !!(file_attributes & FILE_ATTRIBUTE_DIRECTORY);
#else /* _WIN32 */
  struct stat info;
  if (stat(path, &info) == 0)
    return static_cast<bool>(info.st_mode & S_IFDIR);
#endif /* _WIN32 */
  return false;
}

// private

void File::GetInfo ()
{
#ifdef _WIN32

  size_ = static_cast<size_t>(GetFileSize(handle_, NULL));
  FILETIME write_time;
  if (GetFileTime(handle_, NULL, NULL, &write_time)) {
     ULARGE_INTEGER ull;
     ull.LowPart = write_time.dwLowDateTime;
     ull.HighPart = write_time.dwHighDateTime;

     time_ =  ull.QuadPart / 10000000ULL - 11644473600ULL;
  }

#else /* _WIN32 */

  struct stat file_info;
  fstat(handle_, &file_info);
  size_ = file_info.st_size;
  time_ = file_info.st_mtime;

#endif /* _WIN32 */
}

void File::CreateTemp ()
{
#ifdef _WIN32

  //TODO(matthewtff): Implement.
  return;

#else /* _WIN32 */

  char tmp_name[] = "./XXXXXX";
  handle_ = mkstemp(tmp_name);
  if (handle_ == -1) {
    LOG << "Could not create temp file named " << tmp_name << std::endl;
    return;
  }
  name_ = tmp_name;

#endif /* _WIN32 */
  opened_ = true;
}

}  // namespace koohar
