#ifndef koohar_filemapping_hh
#define koohar_filemapping_hh

#include "base/file.hh"

namespace koohar {

class FileMapping {
public:
  explicit FileMapping(const File& file);
  ~FileMapping();
  char* Map(const size_t size, const size_t offset);
  void UnMap();

private:
  const File::Handle file_;
  size_t size_ = 0u;
  char* address_ = nullptr;
  bool mapped_ = false;

#ifdef _WIN32

  HANDLE file_map_;

#endif // _WIN32
};  // class FileMapping

}  // namespace koohar

#endif  // koohar_filemapping_hh
