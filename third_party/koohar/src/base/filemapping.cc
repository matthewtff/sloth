#include "base/filemapping.hh"

#ifdef _WIN32

#include <windows.h>

#else /* _WIN32 */

#include <sys/mman.h>

#endif /* _WIN32 */

namespace koohar {

FileMapping::FileMapping (const File& file) : file_(file.GetHandle()) {
}

FileMapping::~FileMapping() {
	if (mapped_)
		UnMap();
}

char* FileMapping::Map(const size_t size, const size_t offset) {
  // TODO(matthewtff): WTF? remove this offset cheating...
	const size_t align_size = offset;
	size_ = size + align_size;
#ifdef _WIN32

  file_map_ = CreateFileMapping(file_,
                                NULL,
                                PAGE_READONLY,
                                0,
                                size_,
                                NULL);

  if (file_map_ == NULL) {
    return nullptr;
  }

	if (created_map) {
    address_ = static_cast<char*>(MapViewOfFile(file_map_,
                                                FILE_MAP_READ,
                                                0,
                                                offset,
                                                size_));
		if (!address_) {
			CloseHandle(file_map_);
			return nullptr;
		}
  }

#else /* _WIN32 */

  address_ =
      static_cast<char*>(mmap(0, size_, PROT_READ,
                              MAP_PRIVATE, file_, offset));
	if (address_ == MAP_FAILED) {
		return nullptr;
  }

#endif /* _WIN32 */
	mapped_ = true;
	return address_ + align_size;
}

void FileMapping::UnMap () {
#ifdef _WIN32

	UnmapViewOfFile(static_cast<LPVOID>(address_));
	CloseHandle(file_map_);

#else /* _WIN32 */

	munmap(static_cast<void*>(address_), size_);

#endif /* _WIN32 */
	mapped_ = false;
}

}  // namespace koohar
