#include "http_connection.hh"

#include <functional>
#include <sstream>

#include "base/file.hh"
#include "base/utils.hh"
#include "response.hh"
#include "static_transfer.hh"

namespace koohar {

HttpConnection::Pointer HttpConnection::Create(
    asio::io_service& io_service,
    UserFunc user_call_function,
    const ServerConfig& config) {
  return Pointer(new HttpConnection(io_service,
                                    user_call_function,
                                    config));
}

void HttpConnection::Start() {
  socket_.async_read_some(
      asio::buffer(request_buffer_, kMaxRequestSize),
      std::bind(&HttpConnection::HandleRead,
                shared_from_this(),
                std::placeholders::_1,
                std::placeholders::_2));
}

void HttpConnection::Write(const char* data, const std::size_t size) {
  if (!data || !size) {
    LOG << "HttpConnection[write_error]: Bad data or size received";
    return;
  }

  if (close_socket_) {
    LOG << "HttpConnection[write_error]: Writing to socket after close";
    NOTREACHED();
  }

  buffers_.emplace_back(data, data + size);

  if (writing_operations_ == 0) {
    SendNextBuffer();
  }

  ++writing_operations_;
}

void HttpConnection::SetUserFunction(UserFunc user_call_function) {
  user_call_function_ = user_call_function;
}

// private

HttpConnection::HttpConnection(asio::io_service& io_service,
                               UserFunc user_call_function,
                               const ServerConfig& config)
    : config_(config),
      socket_(io_service),
      user_call_function_(user_call_function) {
}

void HttpConnection::SendNextBuffer() {
  asio::async_write(
      socket_,
      asio::buffer(*(buffers_.begin())),
      std::bind(&HttpConnection::HandleWrite,
                shared_from_this(),
                std::placeholders::_1,
                std::placeholders::_2));
}

void HttpConnection::HandleRead(const std::error_code& error,
                                const std::size_t bytes_transferred) {
  if (error) {
    LOG << "HttpConnection[read_error] : " << error.message() << std::endl;
    return;
  }

  Response res {shared_from_this()};
  res.Header("Server", "koohar");

  if (!request_.Update(request_buffer_, bytes_transferred)) {
    res.WriteHead(request_.ErrorCode());
    res.End();
    return;
  } else if (!request_.IsComplete()) {
    return Start();
  }

  if (config_.IsStaticUrl(request_)) {
    StaticTransfer static_transfer {std::move(request_),
                                    std::move(res),
                                    config_};
    static_transfer.Serve();
  } else if (user_call_function_) {
    user_call_function_(std::move(request_), std::move(res));
  } else {
    LOG << "HttpConnection[callback_error] : Callback was not set" <<
        std::endl;
  }
}

void HttpConnection::HandleWrite (const std::error_code& error,
                                  const std::size_t /* transferred */) {
  if (error) {
    LOG << "HttpConnection[write_error] : " << error.message() << std::endl;
  }

  buffers_.pop_front();

  if (writing_operations_ > 0) {
    --writing_operations_;
  } else {
    NOTREACHED();
  }
  if (close_socket_ && writing_operations_ == 0) {
    socket_.close();
  } else {
    SendNextBuffer();
  }
}

}  // namespace koohar
