#include "server_asio.hh"

#include <functional>

#include "base/utils.hh"

using asio::ip::tcp;

namespace koohar {

ServerAsio::ServerAsio(const unsigned short port)
    : port_(port),
      acceptor_(io_service_, tcp::endpoint(tcp::v4(), port)) {
}

void ServerAsio::Listen(HttpConnection::UserFunc user_call_function) {
  user_call_function_ = user_call_function;
  if (Accept()) {
    io_service_.run();
  }
}

// private

bool ServerAsio::Accept() {
  HttpConnection::Pointer connection =
      HttpConnection::Create(acceptor_.get_io_service(),
                             user_call_function_,
                             *this);

  try {
    acceptor_.async_accept(
        connection->Socket(),
        std::bind(&ServerAsio::HandleAccept,
                  this,
                  connection,
                  std::placeholders::_1));
  } catch (std::exception& e) {
    LOG << "Error accepting: " << e.what() << std::endl;
    return false;
  }
  return true;
}

void ServerAsio::HandleAccept(HttpConnection::Pointer connection,
                              const std::error_code& error) {
  if (error) {
    LOG << "ServerAsio[handle_accept] : " << error.message() << std::endl;
    return;
  }

  if (!Accept()) {
    LOG << "ServerAsio[call_accept] : Could not start accepting." <<
        std::endl;
  }
  connection->SetUserFunction(user_call_function_);
  connection->Start();
}

} // namespace koohar
