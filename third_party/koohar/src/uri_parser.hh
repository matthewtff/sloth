#ifndef koohar_uri_parser_hh
#define koohar_uri_parser_hh

#include <string>

#include "base/utils.hh"

namespace koohar {

class UriParser {
 public:
  UriParser() = default;
  UriParser(UriParser&&) = default;
  virtual ~UriParser() = default;

  UriParser& operator=(UriParser&&) = default;

  bool Parse(const std::string& uri);
  std::string Scheme() const { return scheme_; }
  std::string Authority() const { return authority_; }
  std::string Path() const { return path_; }
  std::string Query() const { return query_; }
  std::string Fragment() const { return fragment_; }

  std::string Body(const std::string& QueryName);

 protected:
  void ParseQuery(const std::string& QueryString);

  StringMap queries_;

 private:
  std::string scheme_;
  std::string authority_;
  std::string path_;
  std::string query_;
  std::string fragment_;
};  // class UriParser

}  // namespace koohar

#endif  // koohar_uri_parser_hh
