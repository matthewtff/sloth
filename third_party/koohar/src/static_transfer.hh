#ifndef koohar_static_transfer_hh
#define koohar_static_transfer_hh

#include "request.hh"
#include "response.hh"
#include "server_config.hh"

namespace koohar {

class StaticTransfer {
 public:
  StaticTransfer(Request&& request,
                 Response&& response,
                 const ServerConfig& config);
  void Serve();

 public:
  static const StringMap mime_types_;

 private:
  bool IsVulnerable(const std::string& FileName);
  void HandleError(const unsigned short Code);
  std::string MimeFromName(const std::string& FileName);

 private:
  Request request_;
  Response response_;
  ServerConfig config_;
};  // class StaticTransfer

}  // namespace koohar

#endif  // koohar_static_transfer_hh
