#include "server_config.hh"

#include <algorithm>
#include <functional>
//#include <cstdlib>
#include <stdexcept>


#include "base/file.hh"
#include "base/json.hh"
#include "base/utils.hh"
#include "request.hh"

namespace koohar {

bool ServerConfig::IsStaticUrl(const Request& req) const {
  return std::any_of(static_urls_.cbegin(),
                     static_urls_.cend(),
                     std::bind(&Request::Corresponds,
                               &req,
                               std::placeholders::_1));
}

void ServerConfig::Load(const std::string& file_name) {
  File file(file_name);
  if (!file.Open(File::AccessType::ReadOnly)) {
    return;
  }
  JSON::Object config = JSON::Parse(file.ReadToString());

  if (config["public_dir"].HasType(JSON::Type::String))
    SetStaticDir(config["public_dir"].GetString());

  if (config["use_ssl"].HasType(JSON::Type::Boolean))
    SetUseSSL(config["use_ssl"].GetBoolean());

  if (config["public_urls"].HasType(JSON::Type::Array)) {
    for (const JSON::Object& url : config["public_urls"].GetArray()) {
      if (url.HasType(JSON::Type::String))
        SetStaticUrl(url.GetString());
    }
  }

  if (config["error_pages"].HasType(JSON::Type::Array)) {
    for (JSON::Object& page : config["error_pages"].GetArray()) {
      if (!page.HasType(JSON::Type::Collection))
        continue;

      if (page["code"].HasType(JSON::Type::Integer) &&
          page["path"].HasType(JSON::Type::String)) {
        const long code = page["code"].GetIntegral();
        error_pages_[code] = page["path"].GetString();
      }
    }
  }
}

std::string ServerConfig::GetErrorPage(const unsigned short code) const {
  try {
    return error_pages_.at(code);
  } catch (std::out_of_range& e) {
    return std::string();
  }
}

} // namespace koohar
